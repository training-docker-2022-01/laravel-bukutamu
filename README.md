## Aplikasi Buku Tamu API ##

Fitur :

1. Insert data ke tabel `tamus` dengan melakukan http `POST` ke http://localhost:8000/api/tamu 
2. Ambil data dari tabel `tamus` dengan melakukan http `GET` ke http://localhost:8000/api/tamu 

## Persiapan Database MySQL ##

1. Jalankan `docker compose`

    ```
    docker compose up
    ```

2. Connect ke MySQL (passwordnya : `bukutamu123`)

    ```
    mysql -h 127.0.0.1 -u bukutamu -p bukutamudb
    ```

3. Jalankan migrasi database untuk membuat tabel (bila versi PHP di laptop sudah versi 8)

    ```
    php artisan migrate
    ```

    Bila di laptop belum terinstal PHP versi 8, kita bisa build dengan custom PHP 8 CLI yang sudah ditambahi extension `pdo_mysql`

    ```
    docker build -t php-cli-pdo-mysql -f dockerfile-php-pdo-mysql .
    ```

    Setelah itu, baru jalankan migrasi dengan custom image tersebut

    ```
    docker run -it --rm -e DB_HOST=192.168.1.90  -v "$PWD":/opt -w /opt php-cli-pdo-mysql php artisan migrate
    ```

    Jangan lupa mengganti environment variable `DB_HOST` sesuai dengan alamat IP laptop kita.

    Bila terjadi error `/opt/vendor not found`, ini disebabkan karena project `laravel-bukutamu` baru saja diclone, dan belum ada dependensi library yang biasanya ada di folder `vendor`. Solusinya, jalankan perintah berikut untuk mengunduh dependensi

    ```
    docker run -it --rm -v "$PWD":/opt -w /opt composer:2 composer install
    ```

    Untuk windows, perintahnya seperti ini

    ```
    docker run -it --rm -v %cd%:/opt -w /opt composer:2 composer install
    ```

4. Cek tabel yang dibuat

    ```
    show tables;
    show create table tamus;
    ```

5. Insert sample data ke tabel `tamus`

    ```sql
    insert into tamus (email, nama, created_at, updated_at) values ('endy@muhardin.com', 'Endy Muhardin', current_timestamp(), current_timestamp());
    ```

6. Cek datanya

    ```sql
    select * from tamus;
    ```

7. Mematikan database dan menghapus container (tidak menghapus data)

    ```
    Ctrl-C
    docker compose down
    ```

8. Menghapus database berikut datanya (hapus folder `db-bukutamu`)

    ```
    rm db-bukutamu
    ```

## Build dan Run Aplikasi dengan Docker Compose ##

1. Build dulu aplikasinya

    ```
    docker compose build
    ```

2. Jalankan aplikasi dan database sekaligus

    ```
    docker compose up
    ```

3. Kalau database masih kosong, kita bisa menjalankan migrasi database

    ```
    docker compose exec aplikasi-bukutamu php artisan migrate
    ```

4. Aplikasi bisa diakses di `http://localhost:2222/api/tamu`

5. Shutdown aplikasi

    ```
    Ctrl-C
    docker compose down
    ```

## Menjalankan Image Tanpa Docker Compose ##

1. Build dulu imagenya

    ```
    docker build -t laravel-bukutamu .
    ```

2. Jalankan database dengan `docker run`. Bisa juga menggunakan database terinstal (harus bikin database berikut user untuk mengaksesnya)

    ```
    docker run --rm -p 3306:3306 -e MYSQL_RANDOM_ROOT_PASSWORD=yes -e MYSQL_DATABASE=bukutamudb -e MYSQL_USER=bukutamu -e MYSQL_PASSWORD=bukutamu123 mysql:8
    ```

3. Migrasi database

    ```
    docker run -it --rm -e DB_HOST=192.168.1.90  -v "$PWD":/opt -w /opt php-cli-pdo-mysql php artisan migrate
    ```

3. Jalankan aplikasi dengan `docker run`. Koneksi database diarahkan ke IP host

    ```
    docker run --env DB_HOST=192.168.1.90 --rm -p 7070:80 -v $PWD/laravel-logs:/var/www/html/storage/logs laravel-bukutamu
    ```

4. Test browse ke http://localhost:7070/api/tamu

## Checklist Build Docker Image ##

1. Pastikan proses build berjalan dengan sukses di PC/Laptop kita sendiri
2. Test jalankan image tanpa `docker compose` / secara mandiri

    * database bisa tetap dijalankan dari docker compose
    * database bisa juga menggunakan database yang terinstal
    * database bisa menggunakan `docker run`

## Push Image ke Docker Registry ##

1. Login dulu di command line

    ```
    docker login
    ```

2. Tag ulang image, tambahkan namespace

    ```
    docker tag laravel-bukutamu endymuhardin/laravel-bukutamu
    ```

3. Push image yang sudah diberi tag dengan namespace

    ```
    docker push endymuhardin/laravel-bukutamu
    ```

4. Cek di [Docker Registry](https://hub.docker.com/repositories) untuk memastikan image sudah masuk

## Menjalankan Docker Compose dengan Image dari Registry ##

1. Jalankan file `docker-compose-from-registry.yml`

    ```
    docker compose -f docker-compose-from-registry.yml up
    ```

2. Migrasi database

    ```
    docker compose -f docker-compose-from-registry.yml exec aplikasi-bukutamu php artisan migrate
    ```

3. Test browse ke http://localhost:2222/api/tamu

## Troubleshooting ##

1. Mendapatkan pesan error

    [![error-mysql-connection-refused](img/error-mysql-connection-refused.png)](img/error-mysql-connection-refused.png)

    * Penyebab : konfigurasi laravel tidak berhasil direplace dari environment variabel docker. Terlihat dari error message berikut di `laravel.log`
    
    ```
    [previous exception] [object] (PDOException(code: 2002): SQLSTATE[HY000] [2002] Connection refused at /var/www/html/vendor/laravel/framework/src/Illuminate/Database/Connectors/Connector.php:70)
    [stacktrace]
    #0 /var/www/html/vendor/laravel/framework/src/Illuminate/Database/Connectors/Connector.php(70): PDO->__construct('mysql:host=127....', 'bukutamu', 'bukutamu123', Array)
    ```
    
    * Solusi : konfigurasi tidak boleh dicache, kalau ada perintah `php artisan config:cache` dalam `Dockerfile`, maka harus dihapus

2. Mendapatkan pesan error `SQLSTATE[HY000] [1130] Host '172.18.0.3' is not allowed to connect to this MySQL server` pada waktu menjalankan migration ataupun hit url aplikasi.

    [![error-host-not-allowed](img/error-mysql-host-not-allowed.png)](img/error-mysql-host-not-allowed.png)

    * Penyebab : database sudah terisi data dari `run` sebelumnya, tersimpan di folder `db-bukutamu`

    * Solusi : hapus dulu folder `db-bukutamu` supaya database MySQL diinisialisasi ulang

## Deploy Aplikasi di Server Ubuntu ##

1. Menjadi user `root` supaya bisa menginstal

    ```
    sudo -i
    ```

2. Update dan upgrade dulu package bawaannya

    ```
    apt update && apt upgrade -y
    ```

3. Setup Repository Docker

    ```
    sudo apt-get update
    sudo apt-get install ca-certificates curl gnupg lsb-release
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    ```

4. Install Docker

    ```
    apt-get update && apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin docker-compose -y
    ```

5. Download `docker-compose.yml` dan `docker-compose-production.yml`

    ```
    wget -c -O docker-compose.yml "https://gitlab.com/training-docker-2022-01/laravel-bukutamu/-/raw/main/docker-compose.yml?inline=false"
    wget -c -O docker-compose-production.yml "https://gitlab.com/training-docker-2022-01/laravel-bukutamu/-/raw/main/docker-compose-production.yml?inline=false"
    ```

6. Jalankan aplikasi

    ```
    docker-compose -f docker-compose.yml -f docker-compose-production.yml up -d
    ```



## Referensi ##

* [Membuat docker image production](https://www.honeybadger.io/blog/laravel-docker-php/)
* [Membuat Helm Chart dari Deployment](https://medium.com/geekculture/helm-in-kubernetes-part-2-how-to-create-a-simple-helm-chart-af899fc2741d)
* [Membuat deployment descriptor k8s](https://saurabhkharkate05.medium.com/helm-chart-for-wordpress-php-application-and-mysql-database-on-k8s-c7c553cc610f)
* [Membuat helm chart untuk aplikasi dengan dependensi database](https://tanzu.vmware.com/developer/guides/deploy-spring-boot-application-production-helm/)
* [Tutorial Chart Dependency](https://kodekloud.com/blog/chart-dependencies/)
* [Dokumentasi Resmi Helm Chart dependencies](https://helm.sh/docs/helm/helm_dependency/)
* [Dokumentasi Resmi Helm Chart Values](https://helm.sh/docs/chart_template_guide/values_files/)