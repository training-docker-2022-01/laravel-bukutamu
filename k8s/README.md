# Belajar Kubernetes #

[![Arsitektur Kubernetes](../img/kubernetes.png)](../img/kubernetes.png)

1. Create GKE cluster di Google Cloud
2. Setelah selesai, download credential k8s cluster

    ```
    gcloud container clusters get-credentials belajar-kubernetes --region asia-southeast2 --project training-devops-365802
    ```

3. Cek apakah cluster sudah terkonfigurasi dengan baik

    ```
    kubectl get nodes
    ```

    Harusnya akan tampil daftar nodes seperti ini

    ```
    NAME                                                STATUS   ROLES    AGE   VERSION
    gk3-belajar-kubernetes-default-pool-39e38d50-cgn5   Ready    <none>   16m   v1.23.8-gke.1900
    gk3-belajar-kubernetes-default-pool-98edcde9-p151   Ready    <none>   16m   v1.23.8-gke.1900
    ```

4. Membuat konfigurasi pod untuk MySQL

    ```
    kubectl run db-bukutamu-prod --image=mysql:8 --env="MYSQL_RANDOM_ROOT_PASSWORD=yes" --env="MYSQL_DATABASE=bukutamudb" --env="MYSQL_USER=bukutamu" --env="MYSQL_PASSWORD=bukutamu123" --labels="aplikasi=bukutamu,komponen=db,env=prod" --dry-run=client -o yaml  
    ```

5. Menjalankan deployment MySQL

    ```
    kubectl apply -f 01-mysql.yml
    ```

6. Memantau progress deployment

    ```
    kubectl get deployments
    ```

    Outputnya seperti ini

    ```
    NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
    db-bukutamu-deployment   0/1     1            0           10s
    ```

    Melihat status deployment

    ```
    kubectl rollout status deployment/db-bukutamu-deployment
    ```

    Outputnya seperti ini

    ```
    Waiting for deployment "db-bukutamu-deployment" rollout to finish: 0 of 1 updated replicas are available...
    ```

    Melihat detail deployment

    ```
    kubectl describe deployments
    ```

    Hasilnya sebagai berikut

    ```
    Name:                   db-bukutamu-deployment
    Namespace:              default
    CreationTimestamp:      Thu, 17 Nov 2022 15:08:05 +0700
    Labels:                 aplikasi=bukutamu
                            env=prod
                            komponen=db
    Annotations:            autopilot.gke.io/resource-adjustment:
                            {"input":{"containers":[{"name":"db-bukutamu-prod"}]},"output":{"containers":[{"limits":{"cpu":"500m","ephemeral-storage":"1Gi","memory":"...
                            deployment.kubernetes.io/revision: 1
    Selector:               aplikasi=bukutamu,env=prod,komponen=db
    Replicas:               1 desired | 1 updated | 1 total | 0 available | 1 unavailable
    StrategyType:           RollingUpdate
    MinReadySeconds:        0
    RollingUpdateStrategy:  25% max unavailable, 25% max surge
    Pod Template:
    Labels:  aplikasi=bukutamu
            env=prod
            komponen=db
    Containers:
    db-bukutamu-prod:
        Image:      mysql:8
        Port:       <none>
        Host Port:  <none>
        Limits:
        cpu:                500m
        ephemeral-storage:  1Gi
        memory:             2Gi
        Requests:
        cpu:                500m
        ephemeral-storage:  1Gi
        memory:             2Gi
        Environment:
        MYSQL_RANDOM_ROOT_PASSWORD:  yes
        MYSQL_DATABASE:              bukutamudb
        MYSQL_USER:                  bukutamu
        MYSQL_PASSWORD:              bukutamu123
        Mounts:                        <none>
    Volumes:                         <none>
    Conditions:
    Type           Status  Reason
    ----           ------  ------
    Available      False   MinimumReplicasUnavailable
    Progressing    True    ReplicaSetUpdated
    OldReplicaSets:  <none>
    NewReplicaSet:   db-bukutamu-deployment-56bc967dc7 (1/1 replicas created)
    Events:
    Type    Reason             Age    From                   Message
    ----    ------             ----   ----                   -------
    Normal  ScalingReplicaSet  2m16s  deployment-controller  Scaled up replica set db-bukutamu-deployment-56bc967dc7 to 1
    ```

7. Melihat daftar pods

    ```
    kubectl get pods
    ```

8. Menghapus deployment

    ```
    kubectl delete -f 01-mysql.yml
    ```

9. Generate konfigurasi service untuk MySQL

    ```
    kubectl expose deployment db-bukutamu-deployment --port=3306 --target-port=3306 --dry-run=client -o yaml 
    ```

10. Generate konfigurasi pod untuk aplikasi laravel-bukutamu

    ```
    kubectl run app-bukutamu-prod --image=endymuhardin/laravel-bukutamu:1.0.1-RELEASE --env="DB_HOST=db-bukutamu-service" --labels="aplikasi=bukutamu,komponen=app,env=prod" --dry-run=client -o yaml  
    ```

11. Jalankan deployment untuk aplikasi web

    ```
    kubectl apply -f 02-aplikasi.yml
    ```

12. Generate konfigurasi service untuk aplikasi web

    ```
    kubectl expose deployment app-bukutamu-deployment --port=80 --type=LoadBalancer --dry-run=client -o yaml
    ```

13. Mengecek kondisi pod (misalnya dia `Pending` terus)

    ```
    kubectl describe pod <nama-pod>
    ```

    Outputnya seperti ini

    ```
    Name:             app-bukutamu-deployment-75db95dc96-r47cw
    Namespace:        default
    Priority:         0
    Service Account:  default
    Node:             <none>
    Labels:           aplikasi=bukutamu
                    env=prod
                    komponen=app
                    pod-template-hash=75db95dc96
    Annotations:      seccomp.security.alpha.kubernetes.io/pod: runtime/default
    Status:           Pending
    IP:               
    IPs:              <none>
    Controlled By:    ReplicaSet/app-bukutamu-deployment-75db95dc96
    Init Containers:
    laravel-bukutamu-migration:
        Image:      endymuhardin/laravel-bukutamu:1.0.1-RELEASE
        Port:       <none>
        Host Port:  <none>
        Command:
        php
        Args:
        artisan
        migrate
        --force
        Limits:
        cpu:                500m
        ephemeral-storage:  1Gi
        memory:             2Gi
        Requests:
        cpu:                500m
        ephemeral-storage:  1Gi
        memory:             2Gi
        Environment:
        DB_HOST:  db-bukutamu-service
        Mounts:
        /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-dhtf7 (ro)
    Containers:
    app-bukutamu-prod:
        Image:      endymuhardin/laravel-bukutamu:1.0.1-RELEASE
        Port:       <none>
        Host Port:  <none>
        Limits:
        cpu:                500m
        ephemeral-storage:  1Gi
        memory:             2Gi
        Requests:
        cpu:                500m
        ephemeral-storage:  1Gi
        memory:             2Gi
        Environment:
        DB_HOST:  db-bukutamu-service
        Mounts:
        /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-dhtf7 (ro)
    Conditions:
    Type           Status
    PodScheduled   False 
    Volumes:
    kube-api-access-dhtf7:
        Type:                    Projected (a volume that contains injected data from multiple sources)
        TokenExpirationSeconds:  3607
        ConfigMapName:           kube-root-ca.crt
        ConfigMapOptional:       <nil>
        DownwardAPI:             true
    QoS Class:                   Guaranteed
    Node-Selectors:              <none>
    Tolerations:                 kubernetes.io/arch=amd64:NoSchedule
                                node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                                node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
    Events:
    Type     Reason             Age                 From                                   Message
    ----     ------             ----                ----                                   -------
    Normal   TriggeredScaleUp   4m49s               cluster-autoscaler                     pod triggered scale-up: [{https://www.googleapis.com/compute/v1/projects/training-devops-365802/zones/asia-southeast2-b/instanceGroups/gk3-belajar-kubernetes-default-pool-39e38d50-grp 2->3 (max: 1000)}]
    Warning  FailedScaleUp      4m35s               cluster-autoscaler                     Node scale up in zones asia-southeast2-b associated with this pod failed: GCE quota exceeded. Pod is at risk of not being scheduled.
    Normal   NotTriggerScaleUp  4m                  cluster-autoscaler                     pod didn't trigger scale-up (it wouldn't fit if a new node is added): 2 in backoff after failed scale-up
    Warning  FailedScheduling   0s (x5 over 4m59s)  gke.io/optimize-utilization-scheduler  0/4 nodes are available: 4 Insufficient cpu, 4 Insufficient memory.
    ```

14. Menambah replica dari 1 menjadi 3

    ```
    kubectl scale deployment/app-bukutamu-deployment --replicas=3
    ```

15. Mengaktifkan autoscale

    ```
    kubectl autoscale deployment/app-bukutamu-deployment  --min=1 --max=5 --cpu-percent=80
    ```

16. Mengetes autoscale

    ```
    kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://app-bukutamu-service/api/tamu; done"
    ```

[![Hasil Deployment](../img/deployment-k8s.png)](../img/deployment-k8s.png)


## Referensi ##

* [Membuat konfigurasi kubernetes dengan command line](https://saurabhkharkate05.medium.com/helm-chart-for-wordpress-php-application-and-mysql-database-on-k8s-c7c553cc610f)