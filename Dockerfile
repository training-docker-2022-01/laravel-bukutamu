FROM composer:2 as build
COPY . /app/
RUN composer install --prefer-dist --no-dev --optimize-autoloader --no-interaction

FROM --platform=linux/amd64 php:8-apache as production

ENV APP_ENV=production
ENV APP_DEBUG=false

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions pdo_mysql

RUN docker-php-ext-configure opcache --enable-opcache
COPY docker/php/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

COPY --from=build /app /var/www/html
COPY .env /var/www/html/.env

COPY docker/apache/000-default.conf /etc/apache2/sites-available/000-default.conf

RUN php artisan route:cache && \
    chmod 777 -R /var/www/html/storage/ && \
    chown -R www-data:www-data /var/www/ && \
    a2enmod rewrite